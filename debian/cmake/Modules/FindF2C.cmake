# Copyright 2013 Ettercap Development Team.
#
# Distributed under GPL licnse.
#

# Look for the header file
FIND_PATH(F2C_INCLUDE_DIR NAMES f2c.h)
MARK_AS_ADVANCED(F2C_INCLUDE_DIR)

# Look for the library.
FIND_LIBRARY(F2C_LIBRARY NAMES 
    f2c
)
MARK_AS_ADVANCED(F2C_LIBRARY)

# Make sure we've got an include dir.
if(NOT F2C_INCLUDE_DIR)
  if(F2C_FIND_REQUIRED AND NOT F2C_FIND_QUIETLY)
    message(FATAL_ERROR "Could not find f2c include directory.")
  endif()
  return()
endif()

if(NOT F2C_LIBRARY)
  if(F2C_FIND_REQUIRED AND NOT F2C_FIND_QUIETLY)
    message(FATAL_ERROR "Could not find f2c library.")
  endif()
  return()
endif()

#handle the QUIETLY and REQUIRED arguments and set F2C_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(debian/cmake/Modules/FindF2C.cmake)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(F2C DEFAULT_MSG F2C_LIBRARY F2C_INCLUDE_DIR)

if(F2C_FOUND)
	set(F2C_LIBRARY ${F2C_LIBRARY})
	set(F2C_INCLUDE_DIR ${F2C_INCLUDE_DIR})
        set(F2C_VERSION_STRING ${F2C_VERSION_STRING})
endif(F2C_FOUND)
